package com.rmnivnv.revoluttest.di

import com.rmnivnv.revoluttest.ui.main.MainActivity
import com.rmnivnv.revoluttest.ui.main.MainModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @PerActivity
    @ContributesAndroidInjector(modules = [MainModule::class])
    abstract fun bindMainActivity(): MainActivity

}