package com.rmnivnv.revoluttest.di

import android.app.Application
import android.content.Context
import com.rmnivnv.revoluttest.utils.Flagger
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideAppContext(application: Application): Context = application

    @Provides
    @Singleton
    fun provideFlagger(context: Context) = Flagger(context)

}