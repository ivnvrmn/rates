package com.rmnivnv.revoluttest.utils

import android.util.Log

object Logger {

    fun d(tag: String, message: String) {
        Log.d(tag, message)
    }

    fun e(tag: String, message: String) {
        Log.e(tag, message)
    }

}