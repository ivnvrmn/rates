package com.rmnivnv.revoluttest.utils

import android.content.Context
import com.rmnivnv.revoluttest.R
import javax.inject.Inject

class Flagger @Inject constructor(private val context: Context) {

    fun createUrl(code: String): String {
        return String.format(context.getString(R.string.flag_base_url),
                code.substring(0, code.length - 1))
    }

}