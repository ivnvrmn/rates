package com.rmnivnv.revoluttest.model

data class RateItem(
        val code: String,
        var value: String,
        val flagUrl: String,
        val baseFactor: String
)