package com.rmnivnv.revoluttest.model

data class RatesResponse(
        val base: String,
        val date: String,
        val rates: HashMap<String, String>
)