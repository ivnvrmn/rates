package com.rmnivnv.revoluttest.ui.main

import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rmnivnv.revoluttest.R
import com.rmnivnv.revoluttest.model.RateItem
import com.rmnivnv.revoluttest.model.TEXT_WATCHER_EVENT_KEY
import com.rmnivnv.revoluttest.model.VALUE_KEY
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.rate_item.view.*

class RateAdapter(private val rates: ArrayList<RateItem>,
                  private val clickListener: (RateItem) -> Unit) : RecyclerView.Adapter<RateAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.rate_item, parent, false))


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position == 0) {
            holder.setTextListener()
        }
        holder.bind(rates[position], clickListener)
    }

    override fun getItemCount() = rates.size

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: RateItem, clickListener: (RateItem) -> Unit) = with(itemView) {
            if (item.flagUrl.isNotBlank()) {
                Picasso.get()
                        .load(item.flagUrl)
                        .into(flag_logo)
            }
            country_code.text = item.code
            value.setText(item.value)
            setOnClickListener { clickListener(item) }
        }

        fun setTextListener() = with(itemView) {
            value.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    value.setSelection(p0?.length ?: 0)
                    val intent = Intent(TEXT_WATCHER_EVENT_KEY)
                    intent.putExtra(VALUE_KEY, p0.toString())
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
                }
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            })
        }
    }

}