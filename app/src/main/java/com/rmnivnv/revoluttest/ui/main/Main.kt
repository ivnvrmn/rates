package com.rmnivnv.revoluttest.ui.main

import com.rmnivnv.revoluttest.model.RateItem

interface Main {

    interface View {
        fun updateRates()
        fun updateRates(start: Int, count: Int)
        fun moveItem(from: Int, to: Int)
        fun hideProgress()
    }

    interface Presenter {
        fun onCreate(rates: ArrayList<RateItem>)
        fun onStart()
        fun onStop()
        fun onRateClicked(rate: RateItem)
        fun onTextChanged(value: String?)
    }

}