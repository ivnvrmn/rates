package com.rmnivnv.revoluttest.ui.main

import com.rmnivnv.revoluttest.di.PerActivity
import com.rmnivnv.revoluttest.repository.RatesRepository
import com.rmnivnv.revoluttest.utils.Flagger
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @Provides
    @PerActivity
    fun provideView(mainActivity: MainActivity): Main.View = mainActivity

    @Provides
    @PerActivity
    fun providePresenter(view: Main.View,
                         ratesRepository: RatesRepository,
                         flagger: Flagger): Main.Presenter =
            MainPresenter(view, ratesRepository, flagger)

}