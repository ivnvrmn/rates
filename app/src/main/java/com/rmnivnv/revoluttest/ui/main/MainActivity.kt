package com.rmnivnv.revoluttest.ui.main

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.rmnivnv.revoluttest.R
import com.rmnivnv.revoluttest.model.RateItem
import com.rmnivnv.revoluttest.model.TEXT_WATCHER_EVENT_KEY
import com.rmnivnv.revoluttest.model.VALUE_KEY
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), Main.View {

    @Inject lateinit var presenter: Main.Presenter
    private lateinit var recView: RecyclerView
    private lateinit var adapter: RateAdapter
    private var rates: ArrayList<RateItem> = arrayListOf()
    private val handler: Handler by lazy { Handler() }
    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            if (p1 != null) {
                presenter.onTextChanged(p1.extras.getString(VALUE_KEY))
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupRecycler()
        presenter.onCreate(rates)
    }

    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, IntentFilter(TEXT_WATCHER_EVENT_KEY))
        presenter.onStart()
    }

    private fun setupRecycler() {
        recView = rates_list
        recView.layoutManager = LinearLayoutManager(this)
        adapter = RateAdapter(rates) { presenter.onRateClicked(it) }
        recView.adapter = adapter
    }

    override fun onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver)
        presenter.onStop()
        super.onStop()
    }

    override fun updateRates() {
        adapter.notifyDataSetChanged()
    }

    override fun updateRates(start: Int, count: Int) {
        handler.post {
            adapter.notifyItemRangeChanged(start, count)
        }
    }

    override fun moveItem(from: Int, to: Int) {
        handler.post {
            adapter.notifyItemMoved(from, to)
            recView.smoothScrollToPosition(0)
        }
    }

    override fun hideProgress() {
        progress.visibility = View.GONE
    }
}
