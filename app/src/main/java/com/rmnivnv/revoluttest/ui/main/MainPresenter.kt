package com.rmnivnv.revoluttest.ui.main

import com.rmnivnv.revoluttest.model.RateItem
import com.rmnivnv.revoluttest.model.RatesResponse
import com.rmnivnv.revoluttest.repository.RatesRepository
import com.rmnivnv.revoluttest.utils.Flagger
import com.rmnivnv.revoluttest.utils.Logger
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainPresenter @Inject constructor(
        private val view: Main.View,
        private val ratesRepo: RatesRepository,
        private val flagger: Flagger) : Main.Presenter {

    companion object {
        private const val LOG_TAG = "MainPresenter"
        private const val UPDATE_INTERVAL = 1L
        private const val BASE_DEFAULT = "EUR"
        private const val DEFAULT_VALUE = "1"
    }

    private val disposable = CompositeDisposable()
    private var rates: ArrayList<RateItem> = arrayListOf()
    private var isProgressVisible = true
    private val baseRate: RateItem?
        get() = if (rates.isNotEmpty()) rates[0] else null

    override fun onCreate(rates: ArrayList<RateItem>) {
        this.rates = rates
    }

    override fun onStart() {
        setUpdates()
    }

    private fun setUpdates() {
        disposable.add(Observable.interval(0, UPDATE_INTERVAL, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {getRates(baseRate?.code ?: BASE_DEFAULT,
                        baseRate?.value ?: DEFAULT_VALUE)})
    }

    private fun getRates(base: String, startValue: String) {
        disposable.add(ratesRepo.getLatest(base)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ onRatesArrived(it, startValue) }, { onGetRatesFailed(it) }))
    }

    private fun onRatesArrived(ratesResponse: RatesResponse, baseValue: String) {
        if (isProgressVisible) {
            isProgressVisible = false
            view.hideProgress()
        }
        rates.clear()
        rates.add(RateItem(ratesResponse.base, baseValue, flagger.createUrl(ratesResponse.base), baseValue))
        for ((code, value) in ratesResponse.rates) {
            rates.add(RateItem(code, value, flagger.createUrl(code), value))
        }
        updateRateValues(baseValue)
    }

    private fun updateRateValues(value: String) {
        val baseValue = BigDecimal(value)
        rates.forEachIndexed { index, rateItem ->
            if (index > 0) {
                val baseRateFactor = BigDecimal(rateItem.baseFactor)
                rateItem.value = (baseValue * baseRateFactor).setScale(2, RoundingMode.HALF_EVEN).toPlainString()
            }
        }
        view.updateRates(1, rates.size -1)
    }

    private fun onGetRatesFailed(throwable: Throwable) {
        Logger.e(LOG_TAG, "getRates failed ${throwable.message}")
    }

    override fun onStop() {
        disposable.clear()
    }

    override fun onRateClicked(rate: RateItem) {
        val temp = rates[0]
        val changingPosition = rates.indexOf(rates.find { it.code == rate.code })
        rates[0] = rate
        rates[changingPosition] = temp
        view.moveItem(changingPosition, 0)
    }

    override fun onTextChanged(value: String?) {
        if (value != null) {
            var temp = value
            if (temp.isBlank()) {
                temp = "0"
            }
            rates[0].value = temp
            updateRateValues(temp)
        }
    }

}