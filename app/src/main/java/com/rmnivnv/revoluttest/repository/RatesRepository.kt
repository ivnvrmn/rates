package com.rmnivnv.revoluttest.repository

import com.rmnivnv.revoluttest.model.RatesResponse
import io.reactivex.Single

interface RatesRepository {

    fun getLatest(base: String): Single<RatesResponse>
}