package com.rmnivnv.revoluttest.repository

import com.rmnivnv.revoluttest.model.RatesResponse
import com.rmnivnv.revoluttest.network.RevolutApi
import io.reactivex.Single
import javax.inject.Inject

class RatesRepositoryImpl @Inject constructor(private val revolutApi: RevolutApi) : RatesRepository {

    override fun getLatest(base: String): Single<RatesResponse> {
        return revolutApi.getLatest(base)
    }

}