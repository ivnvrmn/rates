package com.rmnivnv.revoluttest.repository

import com.rmnivnv.revoluttest.network.RevolutApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideRevolutRepository(revolutApi: RevolutApi): RatesRepository = RatesRepositoryImpl(revolutApi)
}