package com.rmnivnv.revoluttest.network

import com.rmnivnv.revoluttest.model.RatesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RevolutApi {

    @GET("latest")
    fun getLatest(@Query("base") base: String): Single<RatesResponse>

}